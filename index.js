window.onload = () => {
  let times = JSON.parse(localStorage.getItem("date_time"));
  let seconds = 0;
  let milliSeconds = 0;
  let totalMilliSeconds = 0;

  let intervalId = null;
  function startTimer() {
    ++totalMilliSeconds;
    seconds = Math.floor(totalMilliSeconds / 100);
    milliSeconds = totalMilliSeconds - seconds * 100;
    document.getElementById("minutes").innerHTML = pad(seconds);
    document.getElementById("seconds").innerHTML = pad(milliSeconds);
  }

  function pad(val) {
    var valString = val + "";
    if (valString.length < 2) {
      return "0" + valString;
    } else {
      return valString;
    }
  }

  document.getElementById("start").addEventListener("click", () => {
    clearInterval(intervalId);
    intervalId = setInterval(startTimer, 10);
  });

  document.getElementById("stop").addEventListener("click", () => {
    if (intervalId) clearInterval(intervalId);
    let time = {
      seconds,
      milliSeconds,
    };
    localStorage.setItem("date_time", JSON.stringify(time));
  });

  document.getElementById("reset").addEventListener("click", () => {
    clearInterval(intervalId);
    totalMilliSeconds = 0;
    document.getElementById("minutes").innerHTML = "00";
    document.getElementById("seconds").innerHTML = "00";
    localStorage.clear();
  });
  if (times) {
    document.getElementById("minutes").innerHTML = `${pad(times?.seconds)}`;
    document.getElementById("seconds").innerHTML = `${pad(
      times?.milliSeconds
    )}`;
  }
  console.log(pad(times?.seconds), pad(times.milliSeconds));
};
